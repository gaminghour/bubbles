let bubble1 =[];

function setup(){
	createCanvas(windowWidth,windowHeight);
	bubble1.push(new Bubble(20,20));

}

function draw(){
	background('grey');
	displayObjects();	
	checkCollision();

	// ellipse(50,50,100,100);
}

function checkCollision(){
	for(var i = 0 ; i< bubble1.length;i++){
		for(var j = 0 ; j< bubble1.length;j++){
			dist = Math.sqrt(
				Math.pow(bubble1[i].x-bubble1[j].x,2) +
				Math.pow(bubble1[i].y-bubble1[j].y,2) 
				);
			sumOfRadii = (bubble1[i].width/2) + (bubble1[j].width/2);
			smallerCircle = (bubble1[i].width >bubble1[j].width)?j:i;
			if(dist<=sumOfRadii && i!=j){
				bubble1.splice(smallerCircle,1);
			}
		}

	}

}

function displayObjects(){
	for(var i = 0 ; i< bubble1.length;i++)
		bubble1[i].move();
}


function mouseDragged(){
	bubble1.push(new Bubble(mouseX,mouseY));
}

class Bubble{

	constructor(x,y){
		this.x=x;
		this.y=y;
		this.width=50;
	}

	move(){
		this.x = this.x + random(-5,5);
		this.y = this.y + random(-5,5);
		this.width = this.width + random(-5,5);

		fill('white');
		noFill();
		ellipse(this.x,this.y,this.width,this.width);
	}

	getCentre(){
		return {
		 h : this.x+ this.width/2,
		 k : this.y+ this.width/2
		};
	}
}